#Использовать cmdline
#Использовать "Модули/.."

Процедура Инициализировать()
	Параметры = ПолучитьПараметры();
	Если Параметры["Команда"] = "sort" Тогда
		МассивЧисел = ПолучитьМассивЧисел(Параметры["Значение"]);		
		Результат = Сортировка.Отсортировать(МассивЧисел);
		Сообщить(СтрСоединить(Результат, ","));	
	Иначе
		ВызватьИсключение "хссссс";
	КонецЕсли;		
КонецПроцедуры

Функция ПолучитьМассивЧисел(ИсходнаяСтрока)
	ТипЧисло = Новый ОписаниеТипов("Число");
	МассивСтрок = СтрРазделить(ИсходнаяСтрока, ",");
	МассивЧисел = Новый Массив;
	Для каждого ЧислоСтрокой Из МассивСтрок Цикл
		МассивЧисел.Добавить(ТипЧисло.ПривестиЗначение(ЧислоСтрокой));
	КонецЦикла;	
	Возврат МассивЧисел;
КонецФункции	

Функция ПолучитьПараметры()
	Парсер = Новый ПарсерАргументовКоманднойСтроки();
	Парсер.ДобавитьПараметр("Команда");
	Парсер.ДобавитьПараметр("Значение");
	
	Параметры = Парсер.Разобрать(АргументыКоманднойСтроки);
	Возврат Параметры;
КонецФункции	

Инициализировать();
